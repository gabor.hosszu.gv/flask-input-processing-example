from flask import render_template, Flask, request
import scoring

app = Flask(__name__)

@app.route('/')
def index():
    # Ez tartalmazza az űrlapot, mindössze csak megmutatjuk itt
    return render_template('index.html')

@app.route('/results', methods=["POST"]) # Itt fontos, megadni hogy POST metódust tudjon fogadni
def results():
    # Ez a rész csinálja a kiértékelés
    # A kiértékelő függvényt a scoring.py-ból hozzuk be
    score = scoring.calculate_score(request.form["text_to_validate"])
    return render_template("results.html", score=score) # Itt a kiszámolt értéket egy paraméterként beadjuk

app.run(debug=True)